#include "rootfinding.h"

void vector_print(char* s,gsl_vector* v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}

int main() {
	int ncalls_system=0, ncalls_rosen=0, ncalls_himmel=0;
    double A = 10000.0;
	void f_system(gsl_vector* p,gsl_vector* fx){
		ncalls_system++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, A*x*y-1);
		gsl_vector_set(fx,1, exp(-x)+exp(-y)-1.0-(1.0/A));
		}

	void f_rosen(gsl_vector* p,gsl_vector* fx){
		ncalls_rosen++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); //df/dx
		gsl_vector_set(fx,1, 200*(y-x*x)); //df/dy
		}

	void f_himmel(gsl_vector* p,gsl_vector* fx){
		ncalls_himmel++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7));
  		gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
		}
    
    printf("Root finding:\n");
    //Solving the system of equations
	gsl_vector* x_system=gsl_vector_alloc(2);
	gsl_vector_set(x_system,0,2);
	gsl_vector_set(x_system,1,10);
	gsl_vector* fx_system=gsl_vector_alloc(2);
	
	printf("Solving the system of equations:\n");
	vector_print("Initial vector x: ",x_system);
	f_system(x_system,fx_system);
	vector_print("            f(x): ",fx_system);
	newton(f_system,x_system,1e-6,1e-3);
	printf("ncalls = %i\n",ncalls_system);
	gsl_vector_fprintf(stderr,x_system,"%g");
	vector_print("      solution x: ",x_system);
	f_system(x_system,fx_system);
	vector_print("            f(x): ",fx_system);

    printf("\n\n");
    //Minimum of the Rosenbrock's function
	gsl_vector* x_rosen=gsl_vector_alloc(2);
	gsl_vector_set(x_rosen,0,-2);
	gsl_vector_set(x_rosen,1,2);
	gsl_vector* fx_rosen=gsl_vector_alloc(2);
	
	printf("Minimum of the Rosenbrock's function:\n");
	vector_print("Initial vector x: ",x_rosen);
	f_rosen(x_rosen,fx_rosen);
	vector_print("            f(x): ",fx_rosen);
	newton(f_rosen,x_rosen,1e-6,1e-3);
	printf("ncalls = %i\n",ncalls_rosen);
	gsl_vector_fprintf(stderr,x_rosen,"%g");
	vector_print("      solution x: ",x_rosen);
	f_rosen(x_rosen,fx_rosen);
	vector_print("            f(x): ",fx_rosen);

    printf("\n\n");
    //Minimum of the Himmelblau's function
	gsl_vector* x_himmel=gsl_vector_alloc(2);
	gsl_vector_set(x_himmel,0,10);
	gsl_vector_set(x_himmel,1,10);
	gsl_vector* fx_himmel=gsl_vector_alloc(2);
	
	printf("Minimum of the Himmelblau's function:\n");
	vector_print("Initial vector x: ",x_himmel);
	f_himmel(x_himmel,fx_himmel);
	vector_print("            f(x): ",fx_himmel);
	newton(f_himmel,x_himmel,1e-6,1e-3);
	printf("ncalls = %i\n",ncalls_himmel);
	gsl_vector_fprintf(stderr,x_himmel,"%g");
	vector_print("      solution x: ",x_himmel);
	f_himmel(x_himmel,fx_himmel);
	vector_print("            f(x): ",fx_himmel);


}

