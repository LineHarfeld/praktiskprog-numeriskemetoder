#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_vector.h>

int ode_driver(void f(int n, double x, double*y, double*dydx), int n, double*xlist, double**ylist, double b, double h, double acc, double eps, int max);
