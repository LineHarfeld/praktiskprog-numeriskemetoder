#include<stdio.h>

void rkstep12(void f(int n, double x, double*y, double*dydx),int n, double x, double*yx, double h, double*yh, double*dy){

    int i; double k0[n], yt[n], k12[n];
    f(n,x,yx,k0);
    for(i=0;i<n;i++) yt[i]=yx[i]+k0[i]*h/2;
    f(n,x+h/2,yt,k12);
    for(i=0;i<n;i++) yh[i]=yx[i]+k12[i]*h;
    for(i=0;i<n;i++) dy[i]=(k0[i]-k12[i])*h/2;
}