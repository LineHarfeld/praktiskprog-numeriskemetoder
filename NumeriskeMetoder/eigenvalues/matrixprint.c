#include <gsl/gsl_matrix.h>
#define FMT "%7.3f"

void matrix_print(gsl_matrix *D){

	for(int r=0;r<D->size1;r++){

		for(int c=0;c<D->size2;c++)fprintf(stdout,FMT,gsl_matrix_get(D,r,c));
		fprintf(stdout,"\n");
	}	
	
printf("\n");
}