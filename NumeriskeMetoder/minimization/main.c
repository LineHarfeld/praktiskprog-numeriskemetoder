#include "minimization.h"

void vector_print(char* s,gsl_vector *v){
	printf("%s",s);
	for(int i=0;i<v->size;i++)printf("%10.4g ",gsl_vector_get(v,i));
	printf("\n");
	}


int main(){
	int ncalls_rosen=0, ncalls_himmel=0;

	void f_rosen(gsl_vector* p,gsl_vector* fx){
		ncalls_rosen++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(1-x)*(-1)+100*2*(y-x*x)*(-1)*2*x); //df/dx
		gsl_vector_set(fx,1, 200*(y-x*x)); //df/dy
	}

	void f_himmel(gsl_vector* p,gsl_vector* fx){
		ncalls_himmel++;
		double x=gsl_vector_get(p,0), y=gsl_vector_get(p,1);
		gsl_vector_set(fx,0, 2*(x*x+y-11)*2*x+2*(x+y*y-7)); 
		gsl_vector_set(fx,1, 2*(x*x+y-11)+2*(x+y*y-7)*2*y);
	}
    
    void hessian_rosen(gsl_vector *v, gsl_matrix *H){
        double x=gsl_vector_get(v,0), y=gsl_vector_get(v,1);

        gsl_matrix_set(H,0,0,2*(600*x*x-200*y+1));
        gsl_matrix_set(H,0,1,-400*x);
        gsl_matrix_set(H,1,0,-400*x);
        gsl_matrix_set(H,1,1,200);
    }

    void hessian_himmel(gsl_vector *v, gsl_matrix *H){
        double x=gsl_vector_get(v,0), y=gsl_vector_get(v,1);

        gsl_matrix_set(H,0,0,12*x*x+4*y-42);
        gsl_matrix_set(H,0,1,4*x+4*y);
        gsl_matrix_set(H,1,0,4*x+4*y);
        gsl_matrix_set(H,1,1,4*x+12*y*y-26);
    }

    //Minimum of the Rosenbrock's function
	gsl_vector* x_rosen=gsl_vector_alloc(2);
	gsl_vector_set(x_rosen,0,-2);
	gsl_vector_set(x_rosen,1,2);
	gsl_vector* fx_rosen=gsl_vector_alloc(2);
	
	printf("Minimum of the Rosenbrock's function:\n");
	vector_print("Initial vector x: ",x_rosen);
	f_rosen(x_rosen,fx_rosen);
	vector_print("            f(x): ",fx_rosen);
	newton(f_rosen,x_rosen,1e-6,1e-3,hessian_rosen);
	printf("ncalls = %i\n",ncalls_rosen);
	//gsl_vector_fprintf(stderr,x_rosen,"%g");
	vector_print("      solution x: ",x_rosen);
	f_rosen(x_rosen,fx_rosen);
	vector_print("            f(x): ",fx_rosen);

    printf("\n\n");
    //Minimum of the Himmelblau's function
	gsl_vector* x_himmel=gsl_vector_alloc(2);
	gsl_vector_set(x_himmel,0,-5);
	gsl_vector_set(x_himmel,1,5);
	gsl_vector* fx_himmel=gsl_vector_alloc(2);
	
	printf("Minimum of the Himmelblau's function:\n");
	vector_print("Initial vector x: ",x_himmel);
	f_himmel(x_himmel,fx_himmel);
	vector_print("            f(x): ",fx_himmel);
	newton(f_himmel,x_himmel,1e-6,1e-3,hessian_himmel);
	printf("ncalls = %i\n",ncalls_himmel);
	//gsl_vector_fprintf(stderr,x_himmel,"%g");
	vector_print("      solution x: ",x_himmel);
	f_himmel(x_himmel,fx_himmel);
	vector_print("            f(x): ",fx_himmel);

    gsl_vector_free(x_rosen);
    gsl_vector_free(fx_rosen);
    gsl_vector_free(x_himmel);
    gsl_vector_free(fx_himmel);

}