#include "montecarlo.h"


double f1(double *x){
    return 1.0/sqrt(x[0]*x[1]); //Integrating from 0 to 1, should be 4
}

double f2(double *x){
    return 4*sqrt(1-((1-x[0])*(1-x[1]))); //Integrating from 0 to 1, should be pi
}

double integral(double *x){
    return (1/(M_PI*M_PI*M_PI)*1/(1-cos(x[0])*cos(x[1])*cos(x[2])));
}

int main(){

int N=1e7;
int dim = 2;

//Funktion f1
double a[] = {0,0};
double b[] = {1,1};
double error = 0;
double result = 0;
plainmc(f1,dim,a,b,N,&result,&error);
printf("1.0/sqrt(x*y) integrated from 0 to 1\n");
printf("Result = %g\n",result);
printf("Error = %g\n",error);
printf("\n\n");

//Function f2
plainmc(f2,dim,a,b,N,&result,&error);
printf("4*sqrt(1-(1-x)²) integrated from 0 to 1\n");
printf("Result = %g\n",result);
printf("Error = %g\n",error);
printf("\n\n");

//Singular integral
int dim2 = 3;

double a2[] = {0,0,0};
double b2[] = {M_PI,M_PI,M_PI};
double error2 = 0;
double result2 = 0;
plainmc(integral,dim2,a2,b2,N,&result2,&error2);
printf("Difficult singular integral\n");
printf("Result = %g\n",result2);
printf("Error = %g\n",error2);

}