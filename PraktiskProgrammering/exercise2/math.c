#include "stdio.h"
#include "math.h"
#include "complex.h"

int main(){

double a=j1(0.5);
printf("bessel=%g\n",a);

double b=tgamma(5.0);
printf("gamma=%g\n",b);

float complex c=csqrt(-2.0);
printf("sqrt(-2)=%g+i%g\n",creal(c),cimag(c));

double  complex d=cexp(I);
printf("e^I=%g+i%g\n",creal(d),cimag(d));

double complex e=cexp(I*M_PI);
printf("e^I*pi=%g+i%g\n",creal(e),cimag(e));

double complex f=cpow(I,M_E);
printf("I^e=%g+i%g\n",creal(f),cimag(f));

float x=0.111111111111111111111111111111;
double y=0.111111111111111111111111111111;
long double z=0.111111111111111111111111111111L;

printf("x=%.25g\n y=%.25lg\n z=%.25Lg\n",x,y,z);
}
