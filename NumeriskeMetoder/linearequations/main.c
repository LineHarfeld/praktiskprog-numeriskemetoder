#include "linearequations.h"

int main(){

int n=4;
int m=3;

fprintf(stdout,"Exercise 1\n");

gsl_matrix *A = gsl_matrix_alloc(n,m);
for(int i=0;i<n;i++){
    for(int j=0; j<m; j++){
        gsl_matrix_set(A,i,j,RND);
    }
}
fprintf(stdout,"\nRandomized tall matrix A =\n"); matrix_print(A);

gsl_matrix *R = gsl_matrix_alloc(m,m);
qr_decomp(A,R);
fprintf(stdout,"\nMatrix Q =\n"); matrix_print(A);
fprintf(stdout,"\nMatrix R =\n"); matrix_print(R);
fprintf(stdout,"R is upper triangular\n");

gsl_matrix *QTQ = gsl_matrix_alloc(m,m);
gsl_blas_dgemm(CblasTrans, CblasNoTrans, 1.0, A, A, 0.0, QTQ);
fprintf(stdout,"\nQ(transposed)Q =\n"); matrix_print(QTQ);

gsl_matrix *QR = gsl_matrix_alloc(n,m);
gsl_blas_dgemm(CblasNoTrans, CblasNoTrans, 1.0, A, R, 0.0, QR);
fprintf(stdout,"\nMatrix QR =\n"); matrix_print(QR);
fprintf(stdout,"\n");
fprintf(stdout,"QR is equal to A\n");

fprintf(stdout,"Exercise 2\n");

gsl_matrix *A1 = gsl_matrix_alloc(n,n);
gsl_matrix *A1_copy = gsl_matrix_alloc(n,n); //copy made for later use, because Q overwrites A
for(int i=0;i<n;i++){
    for(int j=0; j<n; j++){
            gsl_matrix_set(A1,i,j,RND);
            gsl_matrix_set(A1_copy,i,j,gsl_matrix_get(A1,i,j));
    }
}

fprintf(stdout,"\nRandomized square matrix A1 =\n"); matrix_print(A1);

gsl_vector *b = gsl_vector_alloc(n);
for(int i=0; i<n; i++){
    gsl_vector_set(b,i,RND);
}
fprintf(stdout,"Randomized vector b =\n");
gsl_vector_fprintf(stdout,b,"%g");

gsl_matrix *R1 = gsl_matrix_alloc(n,n);
qr_decomp(A1,R1);
//fprintf(stdout,"\nMatrix Q1 =\n"); matrix_print(A1);
//fprintf(stdout,"Matrix R1 =\n"); matrix_print(R1);
//fprintf(stdout,"R1 is upper triangular\n");

gsl_vector *x = gsl_vector_alloc(n);

qr_solve(A1,R1,b,x);

fprintf(stdout,"\nVector x =\n");
gsl_vector_fprintf(stdout,x,"%g");

gsl_vector *b_new = gsl_vector_alloc(n);
gsl_blas_dgemv(CblasNoTrans,1.0,A1_copy,x,0.0,b_new);
fprintf(stdout,"A*x=b_new\n");
fprintf(stdout,"\nVector b_new =\n");
gsl_vector_fprintf(stdout,b_new,"%g");
fprintf(stdout,"\n");
fprintf(stdout,"Ax is equal to b\n");

}