#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void qr_decomp(gsl_matrix*A, gsl_matrix*R);
void qr_backsub(gsl_matrix *R, gsl_vector *x);
void qr_solve(gsl_matrix*Q,gsl_matrix*R,gsl_vector*b,gsl_vector*x);
