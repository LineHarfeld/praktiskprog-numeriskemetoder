#include<assert.h>


double linterp_integ(int n, double *x, double *y, double z){
    
	assert(n>1 && z>=x[0] && z<=x[n-1]);
    
	int i=0, j=n-1; /* binary search: */
	while(j-i>1){

		int m=(i+j)/2;
		if(z>=x[m]) i=m;
		else j=m;
	}

double result = 0;
int k;

for(k=0;k<i;k++){

	double ak = y[k];
	double bk = (y[k+1]-y[k])/(x[k+1]-x[k]);
    result += ak*(x[k+1]-x[k])+(bk/2)*(x[k+1]-x[k])*(x[k+1]-x[k]);
    
}

	double ai = y[i];
	double bi = (y[i+1]-y[i])/(x[i+1]-x[i]);
    result += ai*(z-x[i])+(bi/2)*(z-x[i])*(z-x[i]);
    
    return result;
}