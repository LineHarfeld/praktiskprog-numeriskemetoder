#include<stdio.h>
#include<math.h>
#include<gsl/gsl_errno.h>
#include<gsl/gsl_odeiv2.h>

int diff_equation(double x, const double y[], double dydx[], void *params){

    dydx[0]=(2/sqrt(M_PI))*exp(-(pow(x,2)));

return GSL_SUCCESS;
}

double log(double x){

	gsl_odeiv2_system diff;
	diff.function = diff_equation;
	diff.jacobian = NULL;
	diff.dimension = 2;
	diff.params = NULL;

	double hstart = copysign(0.1,x), epsabs = 1e-6, epsrel = 1e-6;

	gsl_odeiv2_driver *driver =
		gsl_odeiv2_driver_alloc_y_new
			(&diff, gsl_odeiv2_step_rkf45, hstart, epsabs, epsrel);

	double t = 0;
    double y[1] = {0.0};
	gsl_odeiv2_driver_apply (driver,&t,x,y);

	gsl_odeiv2_driver_free (driver);
return y[0];
}

int main(int argc, char *argv[]){
	
	double a,b,dx;
	sscanf(argv[1], "%lf", &a);
	sscanf(argv[2], "%lf", &b);
	sscanf(argv[3], "%lf", &dx);

	for(double x=a; x<=b+0.5*dx;x+=dx)
    {printf("%g\t%g\n",x,log(x));}

return 0;
}