#include "ode.h"

void func(int n, double x, double*y, double*dydx){
    dydx[0]=y[1];
    dydx[1]=-y[0];
return;
}

void expfunc(int n, double x, double* y, double* dydx){
	dydx[0]=y[0];
	dydx[1]=y[0];
	return;
}

int main(){

int n = 2;
int max = 500;

double*xlist = (double*)calloc(max,sizeof(double));
double**ylist = (double**)calloc(max,sizeof(double*));

//Sinus function
for(int i=0;i<max;i++) ylist[i]=(double*)calloc(n,sizeof(double));
double pi=atan(1.)*4;
double a=-2*pi, b=pi, h=0.5, acc=0.01, eps=0.01;

xlist[0]=a;
ylist[0][0]=0;
ylist[0][1]=1;
int k = ode_driver(func,n,xlist,ylist,b,h,acc,eps,max);
//if(k<0) printf("Max steps reached in driver\n");

for(int i=0;i<k;i++) printf("%g %g \n",xlist[i],ylist[i][0]);

printf("\n\n");

for(int i=0;i<k;i++)printf("%g %g\n",xlist[i],sin(xlist[i]));
printf("\n\n");

//Exponential function

double*xlist2 = (double*)calloc(max,sizeof(double));
double**ylist2 = (double**)calloc(max,sizeof(double*));

for(int i=0;i<max;i++) ylist2[i]=(double*)calloc(n,sizeof(double));
//double pi=atan(1.)*4;
double a2=0, b2=10;//, h=0.5, acc=0.01, eps=0.01;

xlist2[0]=a2;
ylist2[0][0]=1;
ylist2[0][1]=10;
int k2 = ode_driver(expfunc,n,xlist2,ylist2,b2,h,acc,eps,max);
//if(k<0) printf("Max steps reached in driver\n");

for(int i=0;i<k2;i++) printf("%g %g \n",xlist2[i],ylist2[i][0]);

printf("\n\n");

for(int i=0;i<k2;i++)printf("%g %g\n",xlist2[i],exp(xlist2[i]));

free(xlist); 
free(ylist);
free(xlist2); 
free(ylist2);
return 0;
}