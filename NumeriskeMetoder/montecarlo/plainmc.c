#include<math.h>
#include<stdlib.h>
#define RND ((double)rand()/RAND_MAX)

void randomx(int dim, double *a, double *b, double *x){
    for(int i=0;i<dim;i++) x[i]=a[i]+RND*(b[i]-a[i]);
}

void plainmc(double f(double*x), int dim, double *a, double *b, int N, double *result, double *error){
   double volume=1; for(int i=0;i<dim;i++) volume*=b[i]-a[i];
   double sum=0,sum2=0, fx, x[dim];
   for(int i=0;i<N;i++){randomx(dim,a,b,x); fx=f(x); sum += fx; sum2 += fx*fx;}
   double mean = sum/N;                            
   double var = sum2/N - mean*mean;    
    *result = mean*volume; *error=sqrt(var/N)*volume;
}