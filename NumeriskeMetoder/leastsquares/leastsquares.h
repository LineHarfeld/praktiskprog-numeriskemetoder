#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_blas.h>

void qr_decomp(gsl_matrix*A, gsl_matrix*R);
void qr_solve(gsl_matrix*Q,gsl_matrix*R,gsl_vector*b,gsl_vector*x);
void qr_inverse(gsl_matrix *A, gsl_matrix *R, gsl_matrix* B);
void lsfit(
	int m, double f(int i,double x),
	gsl_vector* x, gsl_vector* y, gsl_vector* dy,
	gsl_vector* c, gsl_matrix* S);