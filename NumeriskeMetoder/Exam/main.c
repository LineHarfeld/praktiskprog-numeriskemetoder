#include "exam.h"

int secular_calls=0;

double secular_eq(gsl_vector* u, gsl_vector* d, double x){
    
    int n = u->size;
    double fx=1;

    double u_i;
    double d_i;

    for(int i=0; i<n; i++){
        secular_calls+=1;
        u_i=gsl_vector_get(u,i);
        d_i=gsl_vector_get(d,i);

        fx+=u_i*u_i/(d_i-x);
    }
    return fx;
}

double rand_double(double min, double max) 
{
    double range = (max - min); 
    double div = RAND_MAX / range;
    return min + (rand() / div);
}

int main(){
    FILE *dataStream = fopen("calls.txt", "w"); 

    for(int n=6; n<607;n+=30){
    secular_calls=0;
    gsl_matrix* D = gsl_matrix_alloc(n,n);
    gsl_vector* d = gsl_vector_alloc(n);
    gsl_vector* u = gsl_vector_alloc(n);
    gsl_vector* d_sorted = gsl_vector_alloc(n);
    int i,j;
    double rand_number_d;
    double rand_number_u;
    time_t t;
    srand((unsigned) time(&t));
    double rand_min=-10;
    double rand_max=10;
    
        for(i=0;i<n;i++){
            rand_number_d=rand_double(rand_min,rand_max); //Giver et tal mellem -25 og 25
            rand_number_u=rand_double(rand_min,rand_max);
            gsl_matrix_set(D,i,i,rand_number_d);
            gsl_vector_set(d,i,rand_number_d);
            gsl_vector_set(u,i,rand_number_u);
            gsl_vector_set(d_sorted,i,rand_number_d);//Sorterer indgangene i d fra laveste til højeste tal
        }
    gsl_sort_vector(d_sorted);//Sorterer indgangene i d fra laveste til højeste tal

    gsl_vector* search_start = gsl_vector_alloc(n);
    gsl_vector* search_lower = gsl_vector_alloc(n);
    gsl_vector* search_upper = gsl_vector_alloc(n);
    gsl_vector* eigen_values = gsl_vector_alloc(n);

    double mid_interval;

        for(j=0;j<n-1;j++){
            
            mid_interval=(gsl_vector_get(d_sorted,j+1)+gsl_vector_get(d_sorted,j))/2.0;
            gsl_vector_set(search_start,j,mid_interval);
            gsl_vector_set(search_lower,j,gsl_vector_get(d_sorted,j));
            gsl_vector_set(search_upper,j,gsl_vector_get(d_sorted,(j+1)));
        }
    
    double u_dotprod;
    gsl_blas_ddot(u,u,&u_dotprod);

    gsl_vector_set(search_lower,n-1, gsl_vector_get(d_sorted,n-1));
    gsl_vector_set(search_upper,n-1, gsl_vector_get(d_sorted,n-1)+u_dotprod);

    double eps=1e-6, dx=1e-10;

    secular_calls = 0;
    int newton_runs;
    double eigen_value;

    for(int i = 0; i<n; i++){
        newton_runs=0; //number of runs of newton method with different start guess to find correct eigenvalue.
        eigen_value = newton(secular_eq, u, d, gsl_vector_get(search_start,i), dx, gsl_vector_get(search_lower,i), gsl_vector_get(search_upper,i), eps, newton_runs);
        gsl_vector_set(eigen_values,i,eigen_value);
    }
    
    void matrix_print(gsl_matrix *D){
	for(int r=0;r<D->size1;r++){
		for(int c=0;c<D->size2;c++)fprintf(stdout,FMT,gsl_matrix_get(D,r,c));
		fprintf(stdout,"\n");}}

    
    if(n==6){
    fprintf(stdout,"\nThe diagonalized matrix \n"); matrix_print(D);
    printf("\n\n");
    printf("Vector u=\n");
    gsl_vector_fprintf(stdout,u,"%g");
    printf("\n\n");
    printf("Eigenvalues=\n");
    gsl_vector_fprintf(stdout,eigen_values,"%g");
    printf("\n\n");

    
    //Calculating the eigenvalues of vector A using the GSL library
    gsl_matrix* U = gsl_matrix_alloc(n,1); //u-vector on matrix form
    gsl_matrix* C = gsl_matrix_alloc(n,n); //the result of uu^T

    for(i=0;i<n;i++){
    gsl_matrix_set(U,i,0,gsl_vector_get(u,i));
    }

    gsl_blas_dgemm(CblasNoTrans,CblasTrans,1.0,U,U,0.0,C);

    gsl_matrix_add(C,D);

    gsl_vector *eval = gsl_vector_alloc(n);
    gsl_matrix *evec = gsl_matrix_alloc(n,n);

    gsl_eigen_symmv_workspace *workspace = gsl_eigen_symmv_alloc(n); //allocating workspace

    gsl_eigen_symmv(C, eval, evec, workspace);

    gsl_eigen_symmv_free(workspace);

    gsl_eigen_symmv_sort (eval,evec,GSL_EIGEN_SORT_ABS_ASC);

    gsl_sort_vector(eval);
    
    printf("Eigenvalues GLS=\n");
    gsl_vector_fprintf(stdout,eval,"%g");
    
    gsl_vector_free(eval);
    gsl_matrix_free(evec);
    gsl_matrix_free(C);
    gsl_matrix_free(U);
}
fprintf(dataStream,"%i\t%i\n",n,secular_calls);

    gsl_matrix_free(D);
    gsl_vector_free(d);
    gsl_vector_free(u);
    gsl_vector_free(search_start);
    gsl_vector_free(search_lower);
    gsl_vector_free(search_upper);
    gsl_vector_free(eigen_values);

}
}