#include<stdio.h>
#include<math.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_matrix.h>

void newton(void f(gsl_vector* x,gsl_vector* fx), gsl_vector* x, double dx, double eps);
