#include<stdlib.h>
#include<stdio.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#define RND ((double)rand()/RAND_MAX)
#define FMT "%7.3f"
#define max_print 11

int jacobi(gsl_matrix* A, gsl_vector* e, gsl_matrix* V); 

void matrix_print(gsl_matrix *D);