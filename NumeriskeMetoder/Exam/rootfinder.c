#include "exam.h"

double newton(double f(gsl_vector* u,gsl_vector* d, double x), gsl_vector* u, gsl_vector* d, double x, double dx, double x_low, double x_high, double eps, int newton_runs){
	newton_runs+=1;
    //printf("%i\n",newton_runs);
    double Dx;
    double minus_fx;
	double fx;
    double df;
    double y;
    double fy;
    int non_convergent=0;

//while(1){ //This while makes sure the values are within the correct intervals
//    interval_tries++;

    while(1){
		non_convergent++;
        if(non_convergent > 50) break;
	    
        df=(f(u,d,x+dx)-f(u,d,x))/dx;
        minus_fx=-f(u,d,x); //Formel 5 i kapitel: Eigenvalues
        Dx=minus_fx/df;

		double s=2;
//Backtracking
		while(1){
            s*=0.5;
            y=x+Dx*s;
			fy=f(u,d,y);
			if(fabs(fy)<(1.0-s/2.0)*fabs(fx) || s<0.02 ) break;
			}
		x=y;
		fx=fy;
		if(fabs(Dx)<dx || fabs(fx)<eps ) break;
	}

//The last eigenvalue is sometimes in the wrong interval. Search in oppsite half.
    if(x<x_low && newton_runs<10){

        double x_new=(3.0*x_high+1.0*x_low)/4.0;
        double x_mid=(x_low+x_high)/2.0;
        x=newton(f, u, d, x_new, dx, x_mid, x_high, eps, newton_runs);
    }
    else if(x>x_high && newton_runs<10){

        
        double x_new=(3.0*x_low+1.0*x_high)/4.0;
        double x_mid=(x_low+x_high)/2.0;
        x=newton(f, u, d, x_new, dx, x_low, x_mid, eps, newton_runs);
    }

    return x;
}