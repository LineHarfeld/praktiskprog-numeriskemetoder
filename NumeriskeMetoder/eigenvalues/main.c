#include "eigenvalue.h"

int main(int argc, char** argv){

int n=(argc>1? atoi(argv[1]):5);

gsl_matrix *A = gsl_matrix_alloc(n,n);
gsl_matrix *A_copy = gsl_matrix_alloc(n,n);
for(int i=0;i<n;i++) for(int j=i;j<n;j++) {
	double x = RND;
	gsl_matrix_set(A,i,j,x);
	gsl_matrix_set(A,j,i,x);
	}
gsl_matrix_memcpy(A_copy,A);

gsl_matrix *V = gsl_matrix_alloc(n,n);
gsl_vector *e = gsl_vector_alloc(n);

int sweeps = jacobi(A,e,V);

fprintf(stdout,"n=%i, sweeps=%i\n",n,sweeps);

if(n<max_print){
fprintf(stdout,"Random matrix A =\n");
matrix_print(A_copy);
fprintf(stdout,"Matrix A after Jacobi diagnalisation and upper triangular is destroyed =\n");
matrix_print(A);
fprintf(stdout,"Matrix V: the accumulation of the eigenvalues and eigenvectors =\n");
matrix_print(V);
fprintf(stdout,"The eigenvalues =\n");
gsl_vector_fprintf(stdout,e,"%g");
fprintf(stdout,"\n");


gsl_matrix *M_ekstra=gsl_matrix_alloc(n,n);
gsl_matrix *D=gsl_matrix_alloc(n,n);
gsl_matrix *V_copy=gsl_matrix_alloc(n,n);
gsl_matrix_memcpy(V_copy,V);

gsl_blas_dgemm(CblasTrans,CblasNoTrans,1.0,V,A_copy,0.0,M_ekstra);
gsl_blas_dgemm(CblasNoTrans,CblasNoTrans,1.0,M_ekstra,V_copy,0.0,D);

fprintf(stdout,"The diagonal matrix D, with the corresponding eigenvalues =\n");
matrix_print(D);
}
return 0;
}