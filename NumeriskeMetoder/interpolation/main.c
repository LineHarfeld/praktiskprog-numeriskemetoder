#include "interpolation.h"

int main(){

    int n=10;
	double x[n],y[n];

	for(int i=0;i<n;i++){

		x[i]=0.5+i;
		y[i]=0.2*i+RND;
		printf("%g %g\n",x[i],y[i]);
	}

	printf("\n\n");

    double dz=0.1;

	for(double z=x[0];z<=x[n-1];z+=dz){
	
    	double lz=linterp(n,x,y,z);
        printf("%g %g\n",z,lz);

    }

    printf("\n\n");

    for(double z=x[0];z<=x[n-1];z+=dz){
	
    	double integral=linterp_integ(n,x,y,z);
        printf("%g %g\n",z,integral);

    }

return 0;

}