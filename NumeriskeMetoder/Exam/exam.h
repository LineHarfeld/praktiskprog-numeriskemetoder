#include<stdio.h>
#include<stdlib.h>
#include<math.h>
#include<gsl/gsl_matrix.h>
#include<gsl/gsl_vector.h>
#include<gsl/gsl_blas.h>
#include<gsl/gsl_sort_vector.h>
#include<gsl/gsl_eigen.h>
#include<time.h>
#define FMT "%7.3f"

double newton(double f(gsl_vector* u,gsl_vector* d, double x), gsl_vector* u, gsl_vector* d, double x, double dx, double x_low, double x_high, double eps, int newton_runs);