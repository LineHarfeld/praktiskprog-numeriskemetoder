#include <stdio.h>
#include <stdlib.h>
#include <math.h>
#include <gsl/gsl_rng.h>
#include <gsl/gsl_vector.h>

void plainmc(double f(double*x), int dim, double *a, double *b, int N, double *result, double *error);
