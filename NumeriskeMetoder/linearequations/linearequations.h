#include <gsl/gsl_blas.h>
#include <gsl/gsl_vector.h>
#include <gsl/gsl_matrix.h>

#define RND ((double)rand()/RAND_MAX)
//#define FMT "%7.3f"

void qr_decomp(gsl_matrix* A, gsl_matrix* R);

void qr_backsub(gsl_matrix *R, gsl_vector *x);

void qr_solve(gsl_matrix *Q, gsl_matrix *R, gsl_vector *b, gsl_vector *x);

void matrix_print(gsl_matrix *D);